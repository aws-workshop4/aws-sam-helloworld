#https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-getting-started-hello-world.html

sam init
#Step 2 - Build your application
cd sam-app
sam build
#Step 3 - Deploy your application
sam deploy --guided


# test api locally
sam local start-api
curl http://127.0.0.1:3000/hello

# invoke function directly from local
sam local invoke "HelloWorldFunction" -e events/event.json


#clean up
aws cloudformation delete-stack --stack-name sam-app-helloworld --region <<<region>>>
# or,
# sam delete --stack-name sam-app-helloworld